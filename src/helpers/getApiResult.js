module.exports = ({ axios, baseUrl, log }) => async ({ route }) => {
  try {
    const result = await axios.get(`${baseUrl}/${route}`)
    console.log(result)
    return result
  } catch (error) {
    log.error(error)
  }
}
