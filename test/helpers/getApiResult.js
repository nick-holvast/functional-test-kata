const initGetApiResult = require('../../src/helpers/getApiResult')
const constants = require('../../src/constants')

const sandbox = sinon.createSandbox()

describe('getApiResult', () => {
  let axiosGetStub,
    axiosMock,
    logErrorStub,
    logMock

  const baseUrl = constants.api.baseUrl
  const getResult = 'At the party she was kindness in the hard crowd'

  // set up
  beforeEach(() => {
    axiosGetStub = sinon.stub()
    axiosGetStub
      .withArgs(`${constants.api.baseUrl}/${constants.api.paths.jeff}`)
      .resolves(getResult)
    axiosGetStub
      .withArgs(`${constants.api.baseUrl}/${constants.api.paths.steve}`)
      .resolves(getResult)
    axiosGetStub
      .throws(Error('axios stub called with incorrect parameters'))

    axiosMock = {
      get: axiosGetStub
    }

    logErrorStub = sinon.stub()
    logErrorStub.throws(Error('log.error not expected to be called'))
    logMock = {
      error: logErrorStub
    }
  })

  it('should call axios.get with the expected args and return the expected result', async () => {
    const getApiResult = initGetApiResult({
      axios: axiosMock,
      baseUrl,
      log: logMock
    })

    // you could use either path here, they both return the same result
    const result = await getApiResult({
      route: constants.api.paths.steve
    })

    const expectedResult = getResult

    assert.equal(result, expectedResult, 'expected result not returned from get stub')
    assert(
      axiosGetStub.calledOnceWith(`${baseUrl}/${constants.api.paths.steve}`),
      `axiosGetStub not called once with ${baseUrl}/steve`
    )
    // uncomment the below test to check it fails when expected - checking for false positive
    // assert(
    //   axiosGetStub.calledOnceWith(`${baseUrl}/${constants.api.paths.jeff}`),
    //   'axiosGetStub not called once with ${baseUrl}/steve'
    // )
  })

  it('should call log.error when there is an error', async () => {
    logErrorStub.reset()
    const error = new Error('Goat in the API')
    axiosGetStub.reset()
    axiosGetStub.throws(error)

    const getApiResult = initGetApiResult({
      axios: axiosMock,
      baseUrl,
      log: logMock
    })

    // we don't need to return anything here -
    // we aren't checking the return value,
    // just that the log.error function is called
    await getApiResult({
      route: constants.api.paths.jeff
    })

    assert.isTrue(
      logErrorStub.calledOnceWith(error),
      'log.error not called with expected parameters'
    )
  })

  afterEach(() => {
    sandbox.restore()
  })
})
