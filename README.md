# functional-test-kata

Repo for practicing writing tests with function composition.

## Setting up this repo

Clone the repo and install the dependencies with `npm i`.

Bear in mind that this repo doesn't have any runnable source code.
The source code exists solely to run tests against.

## How to use this repo

If you run `npm run test:unit` you will see that we have 0% test coverage.

![Ruh roh](https://media.giphy.com/media/1yMfsRiblWJifHQ8Zq/giphy.gif)

Your job is to fix that. In the file `test/helpers/getApiResult` you'll find a
file set up with two scenarios ready to be written. Fill in the tests until you
get 100% coverage (hint: you can run `npm run test:unit -- --watch` to keep the
tests running).

## The source file

The function we're testing is extremely simple. It takes a `{ route }` parameter
and uses that to hit `axios.get`. It either returns the result or an error. This
is the logic we're testing.

However, before testing this logic we need to _initialise_ the function - if you
look at the function signature, it looks like this:

```javascript
module.exports = ({ axios, baseUrl, log }) => async ({ route }) => (...)
```

In other words, it's a function that returns a function. The first function
call passes all of the function's dependencies, then returns a function that
has access to those dependencies and has a signature of:

```javascript
({ route }) => (...)
```

## Initialising the function

By initialising the function with its dependencies (rather than using `require`)
we take total control in the tests over what we want those dependencies to look
like and how we want them to behave. This is incredibly useful for ensuring we
are only testing the _unit_ in isolation - not testing the dependencies.

In terms of _how_ we do this, it simply involves adding an extra step to the
test. Instead of importing the source file as `getApiResult`, we can import it
as `initGetApiResult` (it's a default export so we can call it anything).

We can then get the function that we actually want to test by initialising the
`init` function with its dependencies, like so:

```javascript
const getApiResult = initGetApiResult({ axios, baseUrl, log })
// bear in mind you may end up doing axios: axiosMock etc
```

We now have the function we want to test in a `const` called `getApiResult` and
can test this as with any function.

## Need help?

Check out the [solutions](https://gitlab.nonprod.dwpcloud.uk/nicholas.holvast/functional-test-kata/-/tree/solutions) branch.
